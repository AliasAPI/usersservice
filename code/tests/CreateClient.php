<?php

namespace AliasAPI\Tests;

use AliasAPI\Client as Client;
use AliasAPI\CrudPair as CrudPair;
use AliasAPI\Messages as Messages;

/**
 * Class MockClient
 * Autoloads files and configures a default request
 *
 * @package AliasAPI\Tests
 */
class CreateClient
{
    public array $request;
    public string $tag;

    /**
     * CreateClient autoloads files and sets additional default request parameters
     *
     * @param array $request
     *
     * @return array
     */
    public function __construct(array $request)
    {
        require_once(dirname(__FILE__) . '/../vendor/aliasapi/frame/client/track.php');

        $this->request = Client\track($request);

        $this->setTag();
    }

    public function getPair(): array
    {
        $pair = CrudPair\get_pair_parameters();

        return $pair;
    }

    public function sendRequest(): array
    {
        //$body = Messages\set_body($this->request);
        $response = Messages\request();

        return $response;
    }

    public function setTag(): string
    {
        $this->tag = Messages\get_request_tag();

        return $this->tag;
    }
}
