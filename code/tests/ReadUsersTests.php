<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;

class ReadItemsTests extends TestCase
{
    // Settings to satisfy Psalm
    protected $backupStaticAttributes = false;
    protected $runTestInSeparateProcess = true;

    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        require_once(dirname(__FILE__) . '/CreateClient.php');
    }

    // public function testReadUserIsNotFound(): void
    // {
    //     $request = [];
    //     $request['action'] = 'create user';
    //     $request['pair']['client'] = 'TestClient';
    //     $request['pair']['server'] = 'UsersService';

    //     $request['users']['user_name'] = 'DoesNotExist';
 
    //     $client = new CreateClient($request);

    //     $response = $client->sendRequest();

    //     $body = $response['body'];

    //     $this->assertEquals('404', $response['status_code']);
    //     $this->assertEquals('Not Found', $response['reason']);
    //     $this->assertEquals($client->tag, $response['tag']);
       
    //     $this->assertEquals('The user has not been created yet.', $body[0]);
    // }


    public function testReadUsers(): void
    {
    //     $request = [];
    //     $request['action'] = 'read items';
    //     $request['pair']['client'] = 'TestClient';
    //     $request['pair']['server'] = 'ItemsService';

    //     $request['items'][] = [
    //         'time' =>
    //         '2020-03-30 17:44:00'
    //     ];
    //     $request['items'][] = [
    //         'uuid' => 'UUID',
    //         'item' => 'item1',
    //         'note' => 'note1',
    //         'time' => '2020-03-30 17:44:00'
    //     ];
    //     $request['items'][] = [
    //         'uuid' => 'UUID',
    //         'item' => 'item2',
    //         'note' => 'note2'
    //     ];

    //     $request['unset'] = ['uuid', 'id'];

    //     $client = new CreateClient($request);

    //     $response = $client->sendRequest();

    //     // sayd($client->tag, $client, $response);
    //     $body = $response['body'];

    //     if (isset($body['items'])
    //         && ! empty($body['items'])) {
    //         $i = 1;
    //         foreach ($body['items'] as $index => $item) {
    //             // Update the row id
    //             $items[$i++] = $item;
    //         }
    //     }

    //     // sayd($response);
    //     $this->assertEquals('200', $response['status_code']);
    //     $this->assertEquals('OK', $response['reason']);
    //     $this->assertEquals($client->tag, $response['tag']);

    //     $this->assertArrayHasKey('item', $items[1]);
    //     $this->assertArrayHasKey('note', $items[1]);
    //     $this->assertEquals('item1', $items[1]['item']);
    //     $this->assertEquals('note1', $items[1]['note']);
    //     $this->assertEquals('2020-03-30 17:44:00', $items[1]['time']);
    //     $this->assertArrayNotHasKey('id', $items[1]);
    //     $this->assertArrayNotHasKey('uuid', $items[1]);

    //     $this->assertArrayHasKey('item', $items[2]);
    //     $this->assertArrayHasKey('note', $items[2]);
    //     $this->assertEquals('item2', $items[2]['item']);
    //     $this->assertEquals('note2', $items[2]['note']);
    //     $this->assertArrayNotHasKey('id', $items[2]);
    //     $this->assertArrayNotHasKey('uuid', $items[2]);
    }
}
