<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;

class DeleteUsersTests extends TestCase
{
    // Settings to satisfy Psalm
    protected $backupStaticAttributes = false;
    protected $runTestInSeparateProcess = true;

    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        require_once(dirname(__FILE__) . '/CreateClient.php');
    }

    public function testDeleteUsers(): void
    {
        $request = [];
        $request['action'] = 'delete user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_name'] = 'DemoAccount';
        $request['users']['view'] = 'delete-user-exert';

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];

        $this->assertEquals('200', $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayHasKey('view', $body);
        $this->assertArrayNotHasKey('user_token', $body);
        $this->assertStringContainsString('delete-user-exit', $body['view']);
        $this->assertContains(
            'The user account has been deleted.',
            $body['replies'][200]
        );
    }
}
