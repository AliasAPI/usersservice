<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;

class CreateUsersTests extends TestCase
{
    // Settings to satisfy Psalm
    protected $backupStaticAttributes = false;
    protected $runTestInSeparateProcess = true;

    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        require_once(dirname(__FILE__) . '/CreateClient.php');
    }
    
    public function testCreateUserEnteredBadValues(): void
    {
        $request = [];
        $request['action'] = 'create user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['email_1'] = '';
        $request['users']['user_name'] = 'No';

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];

        $this->assertEquals('401', $response['status_code']);
        $this->assertEquals('Unauthorized', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayNotHasKey('user_token', $body);

        $this->assertContains(
            'Please enter a user_name with at least 3 characters.',
            $body['replies'][401]
        );
        $this->assertArrayHasKey('401', $body['replies']);
        $this->assertContains(
            'A user_name, email, user_token, or user_uuid is required.',
            $body['replies'][401]
        );
        $this->assertContains(
            'Please enter your best email address.',
            $body['replies'][401]
        );
        $this->assertContains(
            'Please enter a user_name with at least 3 characters.',
            $body['replies'][401]
        );
    }

    public function testCreateUserBadServerIdentityValues(): void
    {
        $request = [];
        $request['action'] = 'create user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_uuid'] = '';

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? $response;

        $this->assertEquals('500', $response['status_code']);
        $this->assertEquals('Internal Server Error', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayNotHasKey('user_token', $body);

        $this->assertArrayHasKey('replies', $body);
        $this->assertArrayHasKey('500', $body['replies']);
        $this->assertContains(
            'The [user_uuid] is not valid.',
            $body['replies'][500]
        );
        $this->assertArrayHasKey('401', $body['replies']);
        $this->assertContains(
            'A user_name, email, user_token, or user_uuid is required.',
            $body['replies'][401]
        );
    }

    public function testCreateUsersRolesAndReferrersMissing(): void
    {
        $request = [];
        $request['action'] = 'create user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_name'] = 'DemoAccount';
        $request['users']['email_1'] = 'email_1@gmail.com';
 
        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];

        $this->assertArrayHasKey('view', $body);
        $this->assertEquals('create-user-error', $body['view']);

        $this->assertArrayNotHasKey('user_uuid', $body);
        $this->assertArrayNotHasKey('user_token', $body);

        $this->assertArrayHasKey(400, $body['replies']);
        $this->assertContains(
            'The user roles is not set in the registration form.',
            $body['replies'][400]
        );
        $this->assertContains(
            'The user referrers is not set in the registration form.',
            $body['replies'][400]
        );
    }

    public function testCreateUsersReferrersNotFound(): void
    {
        $request = [];
        $request['action'] = 'create user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_name'] = 'DemoAccount';
        $request['users']['email_1'] = 'email_1@gmail.com';
 
        $request['users']['roles'] = 'Module&Manager&Marketer&Motivator&Member';
        $request['users']['referrers'] =
            "Mastermind=Not1&" .
            "Module=Not2&" .
            "Manager=Not3&" .
            "Marketer=Not4&" .
            "Motivator=Not5";

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];

        $this->assertEquals('404', $response['status_code']);
        $this->assertEquals('Not Found', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayHasKey('email_1', $body);
        $this->assertArrayHasKey('user_name', $body);

        $this->assertEquals('DemoAccount', $body['user_name']);
        $this->assertEquals('email_1@gmail.com', $body['email_1']);

        $this->assertArrayHasKey('json', $body);
        $this->assertArrayHasKey('date_created', $body);
        $this->assertArrayHasKey('date_modified', $body);
       
        $this->assertArrayNotHasKey('user_uuid', $body);
        $this->assertArrayNotHasKey('user_token', $body);

        $this->assertArrayHasKey('roles', $body);
        $this->assertEquals('Module&Manager&Marketer&Motivator&Member', $body['roles']);

        $this->assertArrayHasKey('referrers', $body);
        $this->assertEquals(
            "Mastermind=Not1" .
            "&Module=Not2&" .
            "Manager=Not3&" .
            "Marketer=Not4&" .
            "Motivator=Not5",
            $body['referrers']
        );

        $this->assertArrayHasKey('deleted', $body);
        $this->assertEquals(0, $body['deleted']);
        
        $this->assertEquals('create-user-error', $body['view']);
        $this->assertContains('The referrer [Not1] is not found.', $body['replies']['404']);
        $this->assertContains('The referrer [Not2] is not found.', $body['replies']['404']);
        $this->assertContains('The referrer [Not3] is not found.', $body['replies']['404']);
        $this->assertContains('The referrer [Not4] is not found.', $body['replies']['404']);
        $this->assertContains('The referrer [Not5] is not found.', $body['replies']['404']);
    }

    public function testCreateUsers(): void
    {
        $request = [];
        $request['action'] = 'delete user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_name'] = 'DemoAccount';
       
        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $request = [];
        $request['action'] = 'create user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_name'] = 'DemoAccount';
        $request['users']['email_1'] = 'email_1@gmail.com';
 
        $request['users']['roles'] = 'Mastermind&Module&Manager&Marketer&Motivator&Member';
        
        // Create Mastermind with fake referrers and make sure they are set to user_name
        $request['users']['referrers'] =
            "Mastermind=Not1" .
            "&Module=Not2" .
            "&Manager=Not3" .
            "&Marketer=Not4" .
            "&Motivator=Not5" .
            "&Member=Not6";

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];
        
        $this->assertEquals('201', $response['status_code']);
        $this->assertEquals('Created', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayHasKey('email_1', $body);
        $this->assertArrayHasKey('user_name', $body);
        $this->assertArrayHasKey('user_uuid', $body);
        $this->assertArrayHasKey('json', $body);
        $this->assertArrayHasKey('date_created', $body);
        $this->assertArrayHasKey('date_modified', $body);
       
        $this->assertEquals('DemoAccount', $body['user_name']);
        $this->assertEquals('email_1@gmail.com', $body['email_1']);

        $this->assertArrayNotHasKey('user_token', $body);

        $this->assertArrayHasKey('roles', $body);
        $this->assertEquals(
            'Mastermind&Module&Manager&Marketer&Motivator&Member',
            $body['roles']
        );
        $this->assertArrayHasKey('referrers', $body);
        $this->assertEquals(
            "Mastermind=DemoAccount" .
            "&Module=DemoAccount" .
            "&Manager=DemoAccount" .
            "&Marketer=DemoAccount" .
            "&Motivator=DemoAccount" .
            "&Member=DemoAccount",
            $body['referrers']
        );

        $this->assertArrayHasKey('deleted', $body);
        $this->assertEquals(0, $body['deleted']);
        
        $this->assertEquals('create-user-exit', $body['view']);
        $this->assertContains(
            'The user account has been created.',
            $body['replies']['201']
        );
    }

    public function testCreateUserAlreadyExists(): void
    {
        $request = [];
        $request['action'] = 'create user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_name'] = 'DemoAccount';
        $request['users']['email_1'] = 'email_1@gmail.com';

        $request['users']['roles'] = 'Mastermind&Module&Manager&Marketer&Motivator&Member';
        $request['users']['referrers'] =
            "Mastermind=Not1" .
            "&Module=Not2" .
            "&Manager=Not3" .
            "&Marketer=Not4" .
            "&Motivator=Not5" .
            "&Member=Not6";

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];

        $this->assertEquals('403', $response['status_code']);
        $this->assertEquals('Forbidden', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayHasKey('email_1', $body);
        $this->assertArrayHasKey('user_name', $body);
 
        $this->assertEquals('email_1@gmail.com', $body['email_1']);
        $this->assertEquals('DemoAccount', $body['user_name']);

        $this->assertArrayHasKey('deleted', $body);
        $this->assertEquals(0, $body['deleted']);
 
        $this->assertArrayNotHasKey('user_uuid', $body);
        $this->assertArrayNotHasKey('user_token', $body);

        $this->assertArrayHasKey('referrers', $body);
        $this->assertEquals(
            "Mastermind=DemoAccount" .
            "&Module=DemoAccount" .
            "&Manager=DemoAccount" .
            "&Marketer=DemoAccount" .
            "&Motivator=DemoAccount" .
            "&Member=DemoAccount",
            $body['referrers']
        );

        $this->assertEquals('create-user-error', $body['view']);
        $this->assertContains(
            'The [email_1@gmail.com] account already exists.',
            $body['replies']['403']
        );
        $this->assertContains(
            'The [DemoAccount] account already exists.',
            $body['replies']['403']
        );
    }
}
