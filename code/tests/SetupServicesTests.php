<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;

class SetupServicesTests extends TestCase
{
    // Settings to satisfy Psalm
    protected $backupStaticAttributes = false;
    protected $runTestInSeparateProcess = true;

    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        require_once(dirname(__FILE__) . '/CreateClient.php');
    }

    public function testSetupServices(): void
    {
        $request = [];
        $request['action'] = 'setup services';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';
        $request['users'] = [];

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];
      
        $this->assertEquals('201', $response['status_code']);
        $this->assertEquals('Created', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);
        $this->assertArrayHasKey('0', $body);
        $this->assertEquals('UsersService setup is complete.', $body[0]);
    }
}
