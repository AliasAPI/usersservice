<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;

class UpdateUsersTests extends TestCase
{
    // Settings to satisfy Psalm
    protected $backupStaticAttributes = false;
    protected $runTestInSeparateProcess = true;

    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');
 
        require_once(dirname(__FILE__) . '/CreateClient.php');
    }

    public function testUpdateUserProfile(): void
    {
        $request = [];
        $request['action'] = 'login user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';
        $request['users']['email_1'] = 'email_1@gmail.com';
        $request['users']['user_sign'] = 'dEmOSiGn';

        $client = new CreateClient($request);
        // Log in to get a valid user_token
        $response = $client->sendRequest();
        $body = $response['body'] ?? [];


        $request = [];
        $request['action'] = 'update user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $user_token = $body['user_token'] ?? '';
        $request['users']['user_token'] = $user_token;
        $request['users']['user_sign'] = 'dEmOSiGn';

        $request['users']['first_name'] = 'Demo';
        $request['users']['last_name'] = 'Account';
        $request['users']['email_1'] = 'email_1@gmail.com';
        $request['users']['email_2'] = 'email_2@gmail.com';
        $request['users']['delay_email'] = 1440;
        $request['users']['phone_mobile'] = '703-555-1212';
        $request['users']['deleted'] =  0;

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];

        $this->assertEquals('200', $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayHasKey('deleted', $body);
        $this->assertEquals(0, $body['deleted']);

        $this->assertArrayHasKey('view', $body);
        $this->assertEquals('update-user-exit', $body['view']);

        $this->assertArrayHasKey('first_name', $body);
        $this->assertEquals('Demo', $body['first_name']);

        $this->assertArrayHasKey('last_name', $body);
        $this->assertEquals('Account', $body['last_name']);

        $this->assertArrayHasKey('phone_mobile', $body);
        $this->assertEquals('703-555-1212', $body['phone_mobile']);

        $this->assertArrayHasKey('user_token', $body);
        $this->assertNotEquals($body['user_token'], $user_token);

        $this->assertArrayHasKey('delay_email', $body);
        $this->assertEquals(1440, $body['delay_email']);

        $this->assertArrayNotHasKey('user_sign', $body);
    }

    public function testLogInUsersEmail2(): void
    {
        $request = [];
        $request['action'] = 'login user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['email_2'] = 'email_2@gmail.com';

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];

        $this->assertEquals('200', $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayHasKey('view', $body);
        $this->assertEquals('login-user-exit', $body['view']);

        $this->assertArrayHasKey('user_uuid', $body);

        $this->assertArrayHasKey('user_token', $body);
        $this->assertStringContainsString('-', $body['user_token']);

        $this->assertArrayHasKey('date_last_login', $body);
        $this->assertGreaterThan('0000-00-00 00:00:00', $body['date_last_login']);

        $this->assertArrayHasKey(200, $body['replies']);
        $this->assertContains(
            'The user is logged in.',
            $body['replies'][200]
        );
        $this->assertContains(
            'Updated table [1] rows.',
            $body['replies'][200]
        );
    }

    public function testLoginTokenDoesNotMatch(): void
    {
        $request = [];
        $request['action'] = 'update user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';
        $expires = time() + 100;
        $request['users']['user_token'] = 'DemoAccount-DoesNotMatch-' . $expires;
        $request['users']['user_sign'] = 'dEmOSiGn';

        $request['users']['first_name'] = 'Demo';
        $request['users']['last_name'] = 'Account';
        $request['users']['email_1'] = 'email_1@gmail.com';
        $request['users']['email_2'] = 'email_2@gmail.com';
        $request['users']['delay_email'] = 1440;
        $request['users']['phone_mobile'] = '703-555-1212';
        $request['users']['deleted'] =  0;

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];

        $this->assertEquals('403', $response['status_code']);
        $this->assertEquals('Forbidden', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayHasKey('user_token', $body);
        $this->assertEquals('', $body['user_token']);

        $this->assertArrayHasKey(403, $body['replies']);
        $this->assertContains(
            'Please retry; The [user_token] token is not valid.',
            $body['replies']['403']
        );
    }


    // public function testUserSignDoesNotMatch(): void
    // {
    //     $request = [];
    //     $request['action'] = 'update user';
    //     $request['pair']['client'] = 'TestClient';
    //     $request['pair']['server'] = 'UsersService';

    //     $request['users']['user_token'] = 'DemoAccount-DoesNotMatch-' . time() + 100;
    //     $request['users']['user_sign'] = 'dEmOSiGn';

    //     $request['users']['first_name'] = 'Demo';
    //     $request['users']['last_name'] = 'Account';
    //     $request['users']['email_1'] = 'email_1@gmail.com';
    //     $request['users']['email_2'] = 'email_2@gmail.com';
    //     $request['users']['delay_email'] = 1440;
    //     $request['users']['phone_mobile'] = '703-555-1212';
    //     $request['users']['deleted'] =  0;

    //     $client = new CreateClient($request);

    //     $response = $client->sendRequest();

    //     $body = $response['body'] ?? [];

    //     $this->assertEquals('403', $response['status_code']);
    //     $this->assertEquals('Forbidden', $response['reason']);
    //     $this->assertEquals($client->tag, $response['tag']);

    //     $this->assertArrayHasKey('user_token', $body);
    //     $this->assertEquals('', $body['user_token']);

    //     $this->assertArrayHasKey(403, $body['replies']);
    //     $this->assertContains(
    //         'Please retry; The [user_token] token is not valid.',
    //         $body['replies']['403']
    //     );
    // }

    // IF User uses update-user-profile-edit AND User is logged in AND user_sign does NOT match
    //     user_sign for login user and update user must match
    //     Error: Please use the same device to update your profile
    //     Delete Cookie  self::deleteCookie($user_id);
    //     https://github.com/panique/huge/blob/40a4380b19221388da7807283db92d4c3e63ae35/application/model/LoginModel.php
    //     Destroy session ID
    //     Log User out
    //     Send User to login-user-error (and display email)

    // IF email_2 does NOT exist AND User is logged in
    //     User is sent to update-user-profile-edit
    //     Error: Please enter an email_2 (for account recovery)

    // IF email_2 is NOT verified AND User is logged in
    //     User is sent to update-user-profile-edit
    //     Error: Please verify the email_2 (for account recovery)


    // IF User updates email AND email_2 (in update-user-profile-edit) AND User is logged in
    //     Error: Please update one email at a time (so that each can be verified)

    // IF email has not been verfied AND User updates email_2
    //     Error: Please verify your primary email before changing email_2



    // How do I know which email WAS being checked?

    // When verification fails, ???

    // date_email_1_verified

    // date_email_2_verified


    // IF User updates email (in update-user-profile-edit) AND is logged in
        // IF email exists and associated with a different user_uuid
            // Error: The [$email] account already exists.
        // IF email_2 has not been verified
            // Error: Please verify email_2 before changing email
        // IF email_1 has not been verified
            // Error: Please verify email_1 before changing email
        // IF user_sign does not match
            // Error:
        // IF there are no errors
            // Update the User’s email
            // Update email_1_verified = 0
            // Set a verifying email flag using user_token tied to email ?
            // An email with a login-user-url is sent to email
            // Log User out
            // Send User to update-user-exit (and display email)




    // IF login succeeds AND email2 is not set

    // Use private paseto token and send email based on account

    // IF login fails OR time expires AND email1_verified = 0 AND email2_verified = 1
        // Error: Please login using your (masked email_2) alternate email

    // IF login fails OR time expires AND email2_verified = 0 AND email1_verified = 1
        // Error: Please login using your (masked email_2) alternate email

    
    // How will the Frontend know the UsersService public key?
    // Should the Frontend send a public key for UsersService to use?


    // IF User updates email_2 (in update-user-profile-edit) AND User is logged in
    //     The User’s email_2 is updated (in UsersService)
    //     A email_2 URL parameter with a random code is added the user_login_url
    //     An email with a login-user-url is sent to email_2
    //     Log User out
    //     Send User to login-user-exit (and display email_2)

    // IF User enters the email_2 (in login-user-edit)
    //     An email with login-user-url is sent to email_2
    //     Destroy session
    //     Log User out
    //     Send User to login-user-exit (and display email_2)

    // IF User goes to login-user-url with correct email_2 code
    //     Update email_2_verified to true (in UsersService)
    //     Regenerate session ID
    //     Log User in

    // IF User goes to login-user-url
    //     Regenerate session ID
    //     Log User in

    // IF User goes to logout-user-url
    //     Destroy User’s session (with session_destroy();)
    //     Log User out
    //     Send User to logout-user-exit

    // IF the User is NOT logged in AND goes to update-user-profile-edit
    //     The User is redirected to login-user-error

    // IF UsersService provides date_last_email & delay_email,
        // Frontend can calculate whether or not to send email

        // What about setting date_last_email ???

    // test: Make sure email and email_2 are unique in the table
}
