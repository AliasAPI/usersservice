<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;
use AliasAPI\CrudTable as CrudTable;

class LoginUsersTests extends TestCase
{
    // Settings to satisfy Psalm
    protected $backupStaticAttributes = false;
    protected $runTestInSeparateProcess = true;

    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        require_once(dirname(__FILE__) . '/CreateClient.php');
    }


    public function testUserDoesNotExist(): void
    {
        $request = [];
        $request['action'] = 'login user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_name'] = 'DemoDontExist';

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];

        $this->assertEquals('404', $response['status_code']);
        $this->assertEquals('Not Found', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayHasKey('user_name', $body);
        $this->assertEquals('DemoDontExist', $body['user_name']);

        $this->assertArrayHasKey('view', $body);
        $this->assertEquals('login-user-error', $body['view']);

        $this->assertArrayNotHasKey('user_uuid', $body);
        $this->assertArrayNotHasKey('user_token', $body);

        $this->assertArrayHasKey('replies', $body);
        $this->assertArrayHasKey(404, $body['replies']);
        $this->assertContains(
            'The [DemoDontExist] account is not found.',
            $body['replies'][404]
        );
    }

    public function testLoginDeletedUserNotFound(): void
    {
        // Delete the User
        $request = [];
        $request['action'] = 'delete user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_name'] = 'DemoAccount';
       
        $client = new CreateClient($request);

        $response = $client->sendRequest();

        // Run the test
        $request = [];
        $request['action'] = 'login user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_name'] = 'DemoAccount';

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];

        $this->assertEquals('404', $response['status_code']);
        $this->assertEquals('Not Found', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayHasKey('user_name', $body);
        $this->assertEquals('DemoAccount', $body['user_name']);

        $this->assertArrayHasKey('view', $body);
        $this->assertEquals('login-user-error', $body['view']);

        $this->assertArrayHasKey('user_token', $body);
        $this->assertEquals('', $body['user_token']);

        $this->assertArrayNotHasKey('user_uuid', $body);

        $this->assertArrayHasKey('replies', $body);
        $this->assertArrayHasKey(404, $body['replies']);
        $this->assertContains(
            'The [DemoAccount] account is not found.',
            $body['replies'][404]
        );

        // Recreate the User (and test the missing roles and referrers)
        $request = [];
        $request['action'] = 'create user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_name'] = 'DemoAccount';
        $request['users']['email_1'] = 'email_1@gmail.com';
 
        $request['users']['roles'] = 'Mastermind&Module&Manager&Marketer&Motivator&Member';
        
        $request['users']['referrers'] =
            "Mastermind=DemoAccount" .
            "&Module=DemoAccount" .
            "&Manager=DemoAccount" .
            "&Marketer=DemoAccount" .
            "&Motivator=DemoAccount" .
            "&Member=DemoAccount";

        $client = new CreateClient($request);

        $response = $client->sendRequest();
    }

    public function testLogInUsers(): void
    {
        $request = [];
        $request['action'] = 'login user';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $request['users']['user_name'] = 'DemoAccount';

        $client = new CreateClient($request);

        $response = $client->sendRequest();

        $body = $response['body'] ?? [];

        $this->assertEquals('200', $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);

        $this->assertArrayHasKey('view', $body);
        $this->assertEquals('login-user-exit', $body['view']);

        $this->assertArrayHasKey('user_uuid', $body);

        $this->assertArrayHasKey('user_token', $body);
        $this->assertStringContainsString('-', $body['user_token']);

        $this->assertArrayHasKey('date_last_login', $body);
        $this->assertGreaterThan('0000-00-00 00:00:00', $body['date_last_login']);

        $this->assertArrayHasKey(200, $body['replies']);
        $this->assertContains(
            'The user is logged in.',
            $body['replies'][200]
        );
        $this->assertContains(
            'Updated table [1] rows.',
            $body['replies'][200]
        );
    }
}
