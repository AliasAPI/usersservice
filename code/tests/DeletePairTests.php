<?php

namespace AliasAPI\Tests;

use PHPUnit\Framework\TestCase;

class DeletePairTests extends TestCase
{
    // Settings to satisfy Psalm
    protected $backupStaticAttributes = false;
    protected $runTestInSeparateProcess = true;

    public function setUp(): void
    {
        // $this->markTestSkipped('Suspend testing.');

        require_once(dirname(__FILE__) . '/CreateClient.php');
    }

    public function testShouldBeTrue(): void
    {
        $this->assertTrue(true);
    }

    public function testDeletePairFile(): void
    {
        // Delete the pair file because it is re-created later
        $request = [];
        $request['action'] = 'delete pair file';
        $request['pair']['client'] = 'TestClient';
        $request['pair']['server'] = 'UsersService';

        $client = new CreateClient($request);

        $response = $client->sendRequest();
       
        $body = $response['body'] ?? [];
        
        $this->assertEquals('200', $response['status_code']);
        $this->assertEquals('OK', $response['reason']);
        $this->assertEquals($client->tag, $response['tag']);
        $this->assertEquals('The pair file has been deleted.', $body[0]);
    }
}
