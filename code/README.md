# README

UsersService

### What is this repository for?

UsersService creates, reads, updates, and deletes users.
It also provides user authentication as a microservice.

### How do I get set up?

- Summary of set up
    Install Docker on Linux
        su
        curl -fsSL https://get.docker.com -o get-docker.sh
        sh ./get-docker.sh
        groupadd --system docker
        usermod -aG docker $USER
        exit
        newgrp docker
        # Prove Docker works:
            docker run hello-world
            docker --version
        su
        curl -L "https://github.com/docker/compose/releases/download/1.28.6/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
        chmod +x /usr/local/bin/docker-compose
        # Configure Docker to start on boot:
            systemctl enable docker.service
            systemctl enable containerd.service

    Post installation steps: https://docs.docker.com/engine/install/linux-postinstall/

    docker inspect ceeaf78d64d4 | grep "IPAddress"

    Add certificates https://github.com/devigner/docker-compose-php/blob/master/docker-compose.yml

    # Get the container ID
    docker ps
    # Get the container IP address
    docker inspect <container id> | grep "IPAddress"
        Users http://172.18.0.4:8080/public
        PHPMyAdmin http://172.18.0.3:9000/

    Get Docker to work with gocryptfs file permissions
        Run gocryptfs as root
        mkdir -p data && sudo chown 1001:root data && sudo chmod 775 data
        docker-compose up -d

        mkdir data/
        chown -R drew:drew data/
        chmod -R a+rwX datafiles/

        userdel -r docker
        useradd -u 1001 -g drew -G root,sudo,daemon -s /bin/bash -c "Provide Docker permissions" docker

    Start UsersService
        cd userservice/code/config 
        docker-compose up -d 
    

- Configuration
- Dependencies
- Database configuration
- How to run tests
- Deployment instructions

### Contribution guidelines

- Writing tests
- Code review
- Other guidelines

### Who do I talk to?

- Repo owner or admin
- Other community or team contact
