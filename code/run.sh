#!/bin/bash
if [[ -d "/opt/lampp/bin/" ]] && [[ -d "/home/drew/ARK/DREWPIX/bin/" ]]; then
    export PATH="$PATH:/opt/lampp/bin/"
    export PATH="$PATH:/home/drew/ARK/DREWPIX/bin"
fi

# Set the alias(es) of the clientside and serverside
clientside="testclient"
serverside="$(basename $(dirname $(pwd)))"

# Use run.sh repair to display and delete the pair files
if [[ $1 == "repair" ]] && [[ -d $(pwd)"/config/$clientside" ]]; then
    ll $(pwd)"/config/$clientside"

    if [[ -f $(pwd)"/config/$clientside"/$clientside"."$serverside ]]; then
        cat $(pwd)"/config/$clientside"/$clientside"."$serverside
        else
            echo "The clientside pair file does not exist yet"
    fi

    rm -rf $(pwd)"/config/$clientside"
fi

if [[ $1 == "repair" ]] && [[ -d $(pwd)"/config/$serverside" ]]; then
    ll $(pwd)"/config/$serverside"

    if [[ -f $(pwd)"/config/$serverside"/$clientside"."$serverside ]]; then
        cat $(pwd)"/config/$serverside"/$clientside"."$serverside
        else
            echo "The serverside pair file does not exist yet"
    fi

    rm -rf $(pwd)"/config/$serverside"
fi

# Use run.sh reinstall to delete the vendor/ directory
if [[ $1 == "reinstall" ]] && [[ -d $(pwd)"/vendor" ]]; then
    rm -rf $(pwd)"/vendor"
fi

# Automatically install the composer packages
if [ ! -d $(pwd)"/vendor" ]; then
    if [ -f $(pwd)"/composer.lock" ]; then
        rm $(pwd)"/composer.lock"
    fi

    composer.phar install --ignore-platform-reqs 
    composer.phar update --with-all-dependencies
fi

if [ "$1" ]; then
    composer.phar lint 

    composer.phar analyze
fi

composer.phar test