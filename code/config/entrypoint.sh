#!/bin/bash

# Dockerfile should create a user named "user" with UID 1001
# Dockerfile should create a group named user with a GID of 1000

# See tag:: entrypoint.sh 

# Set the environment variables
UNAME=user
GNAME=user
DATAFILES_DIR=${APP_DATA_DIR:-../datafiles/}
DATABASES_DIR=${APP_DATA_DIR:-../../data/}

# Change the username to match the one provided
if [ -n "$APP_UNAME" ]; then
    usermod -l $APP_UNAME $UNAME
    UNAME=${APP_UNAME}
fi

# Change the groupname to match the one provided
if [ -n "$APP_GNAME" ]; then
    groupmod -n $APP_GNAME $GNAME
    usermod -g $APP_GNAME $UNAME
    GNAME=${APP_GNAME}
fi

# Change the uid to match the one provided
if [ -n "$APP_UID" ]; then
    usermod -u $APP_UID $UNAME
    chown -R $APP_UID $DATA_DIR/
fi

# Change the gid to match the one provided
if [ -n "$APP_GID" ]; then
    groupmod -g $APP_GID $GNAME
    chgrp -R $APP_GID $DATA_DIR/
    usermod -g $APP_GID $GNAME
fi

# Start it up
su $UNAME -c "exec $@"
su $UNAME -s "/bin/bash" -c "exec php-fpm $OPT_ARGS"
exec "$@"

# Wait indefinately
while true; do
    tail -f /dev/null &
    wait $!
done