<?php

declare(strict_types=1);

namespace AliasAPI\Users;

/**
 *
 *
 * @param   array  $train
 *
 * @return  array  $train
 */
function set_user_deleted(array $train): array
{
    if ($train['action'] === 'delete user') {
        $train['user']['deleted'] = 1;
    } elseif ($train['action'] === 'create user'
              || ! isset($train['user']['deleted'])
              || $train['user']['deleted'] !== 1) {
        $train['user']['deleted'] = 0;
    }

    return $train;
}
