<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Messages as Messages;

/**
 *
 *
 * @param   array   $train
 *
 * @return  array   $train
 */
function set_date_created(array $train): array
{
    if (! \array_key_exists('user_uuid', $train['user'])) {
        return $train;
    }

    if ($train['action'] === 'create user'
        // If the date_created is NOT already set.
        && (! \array_key_exists('date_created', $train['user'])
            || $train['user']['date_created'] == 0)) {
        $train['user']['date_created'] = \gmdate('Y-m-d H:i:s', \time());
    }
    
    if (! \array_key_exists('date_created', $train['user'])
        || $train['user']['date_created'] == 0) {
        Messages\set_reply(501, ["A user date_created has not been set yet."]);
    }

    return $train;
}
