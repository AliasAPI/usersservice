<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Messages as Messages;

/**
 * Set the user's last_name
 *
 * @param   array   $train['user']
 *
 * @return  array   $train['user']['last_name']
 */
function set_user_last_name(array $train): array
{
    if (isset($train['users']['last_name'])) {
        $train['user']['last_name'] = $train['users']['last_name'];
    } elseif (! isset($train['user']['last_name'])) {
        $train['user']['last_name'] = '';
    }

    return $train;
}
