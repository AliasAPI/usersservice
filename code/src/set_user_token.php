<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Check as Check;
use AliasAPI\Crypto as Crypto;
use AliasAPI\Messages as Messages;

/**
 * Checks the request user_token or sets one
 *
 * @param array $train
 *
 * @return array $train
 */
function set_user_token(array $train): array
{
    $logged_in = false;

    // check_token extracts the users token_claims (including id)

    // If the user_token was returned from the database
    if (\array_key_exists('user_token', $train['user'])
        // Get the user_token via a used user_login_url
        && isset($train['users']['user_token'])) {
        $options = [
            // set the database user_token to compare it
            'user_token' => $train['user']['user_token'],
            'check' => 'token',
            'nullable' => false
        ];

        $logged_in = Check\check_token(
            'user_token',
            $train['users']['user_token'],
            $options
        );
    }

    // Create the user_token which is saved in the database for the checks above
    if (($train['action'] === 'login user'
        // If the user_token is still valid, refresh it to extend the login time
        || ($logged_in === true
            && $train['user']['deleted'] !== 1))
        // The user_uuid guarantees the user registered with email and user_name
        && \array_key_exists('user_uuid', $train['user'])) {
        $train['user']['user_token'] =
            Crypto\create_paseto_token(
                $train['alias_attributes']['api_keys']['private_key'],
                $train['token_claims']
            );

        $logged_in = true;

        Messages\set_reply(200, ["The user is logged in."]);
    }

    if ($logged_in === false
        || $train['user']['deleted'] === 1) {
        $train['user']['user_token'] = '';
    }

    return $train;
}
