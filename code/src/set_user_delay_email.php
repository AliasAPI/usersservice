<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Email as Email;
use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/**
 * Sets the number of minutes between date_last_email to date_next_email
 *
 * @param   array  $train  The train['users'] with the user's delay_email
 *
 * @return  array  $train['user']['delay_email']
 */
function set_user_delay_email(array $train): array
{
    if (! \array_key_exists('user_uuid', $train['user'])) {
        return $train;
    }

    if ($train['action'] === 'create user') {
        $train['user']['delay_email'] = 0;
    }

    if (\array_key_exists('delay_email', $train['users'])
        && \is_int($train['users']['delay_email'])) {
        $train['user']['delay_email'] = $train['users']['delay_email'];
    }

    if (! \array_key_exists('delay_email', $train['user'])
        || ! \is_int($train['user']['delay_email'])) {
        Messages\set_reply(501, ["A user delay_email is required for every user."]);
    }

    return $train;
}
