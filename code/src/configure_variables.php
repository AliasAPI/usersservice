<?php

declare(strict_types=1);

namespace AliasAPI\Check;

use AliasAPI\Check as Check;

/**
 * Configures variables to standardize how data is processed throughout the system
 *
 * Get the sanitized, edited, and validated value with Check\get_key_value($name);
 * The input keys are case sensitive.
 *
 * php-src/ext/filter/logical_filters.c
 * https://github.com/php/php-src/blob/master/ext/filter/logical_filters.c#L575
 *
 * @params $train
 *
 * @return array $var
 */
function configure_variables(array $train): array
{
    $var['address_street'] = ['check' => 'regex',
                              'nullable' => false,
                              'regex' => '/^.*(?=.{4,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$/'];
    // address_city
    // address_state
    $var['address_postal_code'] = ['check' => 'postal_code',
                                   'nullable' => false,
                                   'format' => 'US'];
    $var['deleted'] = [
        'check' => 'integer',
        'nullable' => false,
        'range' => [
            'min' => 0,
            'max' => 1
        ]
    ];
    $var['delay_email'] = [
        'check' => 'integer',
        'nullable' => false,
        'range' => [
            'min' => 0,
            // Number of minutes per month
            'max' => 43800
        ]
    ];
    $var['email_1'] = [
        'check' => 'email',
        'nullable' => false
    ];
    $var['email_2'] = [
        'check' => 'email',
        'nullable' => false
    ];
    $var['first_name'] = ['check' => 'name',
                          'nullable' => false,
                          'range' => ['min' => 2, 'max' => 50]];
    $var['last_name'] = ['check' => 'name',
                         'nullable' => false,
                         'range' => ['min' => 2, 'max' => 50]];
    $var['password'] = ['check' => 'password',
                        'nullable' => true];
    $var['phone_mobile'] = ['check' => 'phone',
                            'nullable' => true];
    $var['referrers'] = ['check' => 'regex',
                         'nullable' => true,
                         // The referrers string starts with a letter
                         // It can have letters, numbers, "&", "=" and underscores
                         // Example: Key_01=Value_01&Key_02=Value2&Key3=Value
                         'regex' => '/^([A-Za-z0-9=_\&]+)$/'];
    $var['roles'] = ['check' => 'regex',
                     'nullable' => true,
                     // Letters, numbers, "&" and underscores
                     'regex' => '/^([A-Za-z0-9_\&]+)$/'];

    $var['service_uuid'] = ['check' => 'uuid',
                            'nullable' => false];
    $var['user_name'] = ['check' => 'user_name',
                         'nullable' => false];
    $var['user_sign'] = ['check' => 'string',
                         'nullable' => false,
                         'range' => [
                            'min' => 5,
                            'max' => 255]];
    $var['user_token'] = [
        'check' => 'token',
        'nullable' => true
    ];
    if (isset($train['alias_attributes']['api_keys']['public_key'])) {
        $var['user_token']['public_key'] =
            $train['alias_attributes']['api_keys']['public_key'];
    }

    $var['user_uuid'] = ['check' => 'uuid',
                         'nullable' => false];
    $var['view'] = ['check' => 'regex',
                    'nullable' => false,
                    'regex' => '/^[a-z][a-z0-9-]*/'];

    \ksort($var);
  
    return $var;
}
