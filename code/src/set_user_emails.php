<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Check as Check;
use AliasAPI\Email as Email;
use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/**
 * Critically checks emails that should NOT already be in the database
 *
 * @param array $train
 *
 * @return array $train
 */
function set_user_emails(array $train): array
{
    $user_emails = [];

    if ($train['action'] === 'create user'
        && ! isset($train['users']['email_1'])) {
        Messages\set_reply(400, ["Please enter your best email so that you can log in."]);
    } elseif ($train['action'] === 'create user') {
        $user_emails['email_1'] = $train['users']['email_1'];
    }

    if ($train['action'] === 'update user') {
        if (! isset($train['users']['email_1'])) {
            Messages\set_reply(400, ["Please enter a primary email."]);
        } else {
            $user_emails['email_1'] = $train['users']['email_1'];
        }

        if (! isset($train['users']['email_2'])) {
            Messages\set_reply(400, ["Please enter an alternate email for account recovery."]);
        } elseif ($train['users']['email_2'] === $train['users']['email_1']) {
            unset($train['users']['email_2']);
            Messages\set_reply(403, ["Please enter a different alternate email address."]);
        } elseif (isset($train['user']['email_1'])
                  && $train['users']['email_2'] === $train['user']['email_1']) {
            Messages\set_reply(403, ["Please enter a new alternate email address."]);
        } else {
            $user_emails['email_2'] = $train['users']['email_2'];
        }

        if (! \array_key_exists('user_uuid', $train['user'])) {
            Messages\set_reply(500, ["The user_uuid is required to update a user."]);
        }
    }

    if (($train['action'] === 'create user'
        || $train['action'] === 'update user')
        && \count($user_emails) > 0) {
        foreach ($user_emails as $key => $email) {
            if (Check\check_email($key, $email, [])
                && Email\check_mailchecker($email)
                && Email\check_emailvalidator($email)
                && Email\check_disify($email)) {
                // Query the email_1 & email_2 columns to find duplicate addresses
                $account = CrudTable\read_rows('users', ['email_1' => $email], 1);

                if (\count($account) > 0
                    && isset($account['deleted'])
                    && $account['deleted'] === 0

                    // If a User is being created the email cannot be used
                    && ($train['action'] === 'create user'

                        // The email must NOT be used by a user with a different user_uuid
                        || ($train['action'] === 'update user'
                            && isset($account['user_uuid'])
                            && $account['user_uuid'] !== ''
                            && $account['user_uuid'] !== $train['user']['user_uuid'])
                        )
                    ) {
                    Messages\set_reply(403, ["The [$email] account already exists."]);
                }

                $account = CrudTable\read_rows('users', ['email_2' => $email], 1);

                if (\count($account) > 0
                    && isset($account['deleted'])
                    && $account['deleted'] == 0

                    // If a User is being, created the email cannot be used
                    && ($train['action'] === 'create user'

                        // The email must NOT be used by a user with a different user_uuid
                        || ($train['action'] === 'update user'
                            && isset($account['user_uuid'])
                            && $account['user_uuid'] !== ''
                            && $account['user_uuid'] !== $train['user']['user_uuid'])
                        )
                    ) {
                    Messages\set_reply(403, ["The [$email] account already exists."]);
                }

                // Do NOT set the user email if there are any errors
                if (\count(Messages\get_reply(400, 600, 1)) === 0) {
                    $train['user'][$key] = $email;
                }
            }
        }
    }
    
    if ($train['action'] !== 'delete user'
        && ! \array_key_exists('email_1', $train['user'])) {
        Messages\set_reply(501, ["A valid email has not been saved yet."]);
    }

    return $train;
}
