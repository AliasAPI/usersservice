<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Users as Users;
use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/**
 * Read one User from the users table
 *
 * @param   array  $train
 *
 * @return  array  $train
 */
function read_users_row(array $train): array
{
    $user = [];
    $user_id = '';

    if (isset($train['user'])
        && \count($train['user']) > 0) {
        $user_id = \array_key_first($train['user']);

        $ids = ['user_token', 'user_name', 'user_uuid', 'email_1', 'email_2'];

        if (\in_array($user_id, $ids)) {
            $bind_sql = "SELECT * FROM `users`
                         WHERE `user_token` = :user_token
                         OR `user_name` = :user_name
                         OR `user_uuid` = :user_uuid
                         OR `email_1` = :email_1
                         OR `email_2` = :email_2
                         LIMIT 1";

            $where_pairs = [
                ':user_token' => $train['user'][$user_id],
                ':user_name' => $train['user'][$user_id],
                ':user_uuid' => $train['user'][$user_id],
                ':email_1' => $train['user'][$user_id],
                ':email_2' => $train['user'][$user_id]
            ];

            $statement = CrudTable\query($bind_sql, [], $where_pairs);

            $user = (array) $statement->fetchAll();
          
            if (isset($user[0])
                && \count($user[0]) > 0) {
                $train['user'] = $user[0];
            }
        }
    }

    if ($train['action'] === 'login user'
        && (! isset($train['user']['deleted'])
            || $train['user']['deleted'] === 1)) {
        Messages\set_reply(404, ["The [".$train['user'][$user_id]."] account is not found."]);

        $train['user']['view'] = 'login-user-error';

        list($status_code, $response) = Users\set_response($train);

        Messages\respond($status_code, $response);
    }

    return $train;
}
