<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\CrudJson as CrudJson;
use AliasAPI\Messages as Messages;

/**
 * Checks the User's signature (Javascript fingerprint)
 *
 * @param array $train
 *
 * @return array $train
 */
function set_user_sign(array $train): array
{
    if (isset($train['users']['user_sign'])) {
        // Compare the JSON strings first

        // array_diff using check_array()
        // Compare keys
        // Compare the string values using explode()
        // count() then calculate percentage
        // https://github.com/rogervila/array-diff-multidimensional/blob/master/src/ArrayDiffMultidimensional.php
        $train['user']['user_sign'] = $train['users']['user_sign'];
    } else {
        $train['user']['user_sign'] = '';
    }

    // Browser fingerprinting via favicon!
    // https://github.com/jonasstrehle/supercookie

    // user_sign
        // Check to make sure the fingerprint is only associated with one user
        // using the less strict fingerprinting methodologies, or email error!
        // The error is just to prove the reliability of the fingerprinting
        // OR a user_sign can be forced to be unique by including the user_name
        // it's better to prove the reliability of the user_sign

        // ip_address
        // longitude and latitude not too far away (from last longitude and latitude) based on time
        // browser_user_agent_verified

    // Users cannot have concurrent login sessions
        // user_token may take care of this. Does it?
            // WHEN does the User get logged out for not matching their user_sign?
            // Login link fails if it is reused, because the user_token changed
 
    return $train;
}
