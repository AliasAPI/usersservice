<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Check as Check;
use AliasAPI\Users as Users;
use AliasAPI\SuiteCRM as SuiteCRM;
use AliasAPI\Messages as Messages;

function track(array $train): void
{
    $train = Users\check_train($train);

    $train = Users\create_users_table($train);

    // Check the user_token array after it is decoded
        // Add the elements to configure_variables()

    // Add the public_key to the user_token options here using $train

    $vars = Check\configure_variables($train);

    Check\set_variable_options('variables', $vars);

    $train['users'] = Check\set_inputs($train['users']);

    // todo:: Consider using the token and session to avoid calls to the database! Caching
    // Maybe the user_token can simply be updated if action = 'read user'
    // csrf_token + user_token + user_sign

    // $train = Users\get_user_token($train);
        // $data = Crypto\read_paseto_token($public_key, $user_token);


    $train = Users\set_identity($train);

    $train = Users\read_users_row($train);


    $train = Users\set_user_emails($train);

    $train = Users\set_user_name($train);
    says($train);
    $train = Users\set_user_uuid($train);


    $train = Users\set_user_roles($train);

    $train = Users\set_user_referrers($train);


    $train = Users\set_user_sign($train);


    $train = Users\get_user_json($train);

    $train = Users\set_user_deleted($train);

    $train = Users\set_user_delay_email($train);

    $train = Users\set_date_created($train);
    
    $train = Users\set_date_modified($train);


    // $train = Users\set_date_email_1_verified($train);
        // if (isset() && token_data['sub'] == 'Verify email_1')
    

    // $train = Users\set_date_email_1_verified($train);

    // $train = Users\set_date_email_2_verified($train);



    // user has not clicked log out
 
    $train = Users\set_user_token_claims($train);

    $train = Users\set_user_token($train);

    $train = Users\set_date_last_login($train);

    $train = Users\set_user_first_name($train);
    
    $train = Users\set_user_last_name($train);

    // $train = Users\set_user_phone($train);
        // http://twilio.com $ 0.0075  (Move this cost to the Frontend / Client)
        // 160 character limit
        // Send for free? https://itstillworks.com/send-sms-using-php-7300418.html
        // https://www.makeuseof.com/tag/10-sites-to-send-free-text-messages-to-cell-phones-sms/


    // Login
        // loginWithCookie($cookie) ???
        // https://github.com/panique/huge/blob/40a4380b19221388da7807283db92d4c3e63ae35/application/model/LoginModel.php
 
    // If a users inputs NOT set in user, append to user
    foreach ($train['users'] as $key => $value) {
        if (! isset($train['user'][$key])) {
            $train['user'][$key] = $value;
        }
    }

    $train = Users\set_user_json($train);

    $train = Users\delete_user($train);

    $train = Users\update_user_row($train);

    $train = Users\create_user_row($train);


    $train = Users\set_user_view($train);

    list($status_code, $users) = Users\set_response($train);

    Messages\respond($status_code, $users);
}
