<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/**
 * Mark the user row deleted = true
 *
 * @param   array  $train
 *
 * @return  array  $train
 */
function delete_user(array $train): array
{
    if ($train['action'] === 'delete user'
        && ! \array_key_exists('user_uuid', $train['user'])) {
        Messages\set_reply(200, ["The user account has been deleted."]);

        return $train;
    } elseif ($train['action'] === 'delete user'
              && \strlen($train['user']['user_uuid']) > 0) {
        $update_pairs = [
            'deleted' => $train['user']['deleted'],
            'date_modified' => $train['user']['date_modified'],
            'user_token' => $train['user']['user_token']
        ];

        unset($train['user']['deleted']);
        unset($train['user']['date_modified']);
        unset($train['user']['user_token']);

        $user_account = CrudTable\read_rows(
            'users',
            ['user_uuid' => $train['user']['user_uuid']],
            1
        );

        if (\count($user_account) > 0) {
            CrudTable\update_rows(
                'users',
                $update_pairs,
                ['user_uuid' => $train['user']['user_uuid']]
            );
        }
    
        Messages\set_reply(200, ["The user account has been deleted."]);
    }

    return $train;
}
