<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Check as Check;
use AliasAPI\Messages as Messages;
use AliasAPI\CrudTable as CrudTable;

/**
 * Set the user's user_name
 *
 * The token is used to select from the database and set $train['user'] for 'update user'
 *
 * @param   array  $train['users']['user_name']
 *
 * @return  array  $train
 */
function set_user_name(array $train): array
{
    $account = [];

    if ($train['action'] === 'create user'
        && \array_key_exists('user_name', $train['users'])
        && Check\check_user_name('user_name', $train['users']['user_name'], [])) {
        $user_name = Check\get_key_value('user_name');

        // Check to see if the user_name is already used
        $account = CrudTable\read_rows('users', ['user_name' => $user_name], 1);

        if (\count($account) > 0
            && \array_key_exists('deleted', $account)
            && $account['deleted'] !== 1) {
                $acc = print_r($account, true);
            Messages\set_reply(403, ["The [" . $account['user_name'] . "] account already exists."]);
        }

        // Do NOT allow the user_name to be set if there are any errors
        if (\count(Messages\get_reply(400, 600, 1)) === 0) {
            $train['user']['user_name'] = $user_name;
        }
    }
    
    if ($train['action'] !== 'delete user'
        && (! \array_key_exists('user_name', $train['user'])
            || \strlen($train['user']['user_name']) < 4)) {
        Messages\set_reply(501, ["A valid user_name has not been saved yet."]);
    }

    return $train;
}
