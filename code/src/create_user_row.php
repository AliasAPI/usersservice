<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/**
 * Create one user row in the users table
 *
 * @param   array  $train['user']
 *
 * @return  array  $train
 */
function create_user_row(array $train): array
{
    if ($train['action'] !== 'create user') {
        return $train;
    }

    // Check for errors
    $errors = Messages\get_reply(400, 600, 1);

    if (\count($errors) === 0) {
        $update_pairs = [
            'deleted' => $train['user']['deleted'],
            'date_modified' => $train['user']['date_modified'],
            'user_token' => $train['user']['user_token']
        ];

        $where_pairs = [
            'user_uuid' => $train['user']['user_uuid']
        ];

        // Unset so that accounts can be recreated
        unset($train['user']['deleted']);
        // Unset so that the read_rows() works
        unset($train['user']['date_modified']);
        // Unset since a created users cannot log in
        unset($train['user']['user_token']);

        $account = CrudTable\read_rows(
            'users',
            ['user_uuid' => $train['user']['user_uuid']],
            1
        );

        if (\count($account) !== 0) {
            CrudTable\update_rows('users', $update_pairs, $where_pairs);
        } else {
            $train['user']['date_modified'] = $update_pairs['date_modified'];

            $insert_id = CrudTable\create_row('users', $train['user']);
        }

        Messages\set_reply(201, ["The user account has been created."]);

        $train['user'] = CrudTable\read_rows('users', $where_pairs, 1);
    }

    return $train;
}
