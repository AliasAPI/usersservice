<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\CrudJson as CrudJson;
use AliasAPI\Messages as Messages;

/**
 * Set the final response from UsersService
 *
 * @param   array  $train
 *
 * @return  array  $train
 */
function set_response(array $train): array
{
    // Restore user_json
    if (isset($train['user']['json'])) {
        $data = CrudJson\decode_json($train['user']['json']);

        foreach ($data as $key => $value) {
            if (! isset($train['user'][$key])) {
                $train['user'][$key] = $value;
            }
        }
    }

    $replies = Messages\get_reply(0, 600, 100);

    // Respond with the highest http status code
    $status_code = \array_key_first($replies);
   
    $train['user']['replies'] = $replies;

    // If there are any errors, remove user_uuid
    // so that subsequent servers cannot process
    if (\count(Messages\get_reply(400, 600, 1)) > 0) {
        unset($train['user']['user_uuid']);
    }

    unset($train['user']['user_sign']);

    if ($train['action'] === 'create user') {
        unset($train['user']['user_token']);
    }

    if ($train['action'] !== 'create user') {
        unset($train['user']['json']);
        unset($train['user']['date_created']);
        unset($train['user']['date_modified']);
        unset($train['user']['date_last_email']);
    }
 
    if ($train['action'] === 'login user') {
        unset($train['user']['deleted']);
        unset($train['user']['email_1']);
        unset($train['user']['referrers']);
        unset($train['user']['delay_email']);
    }

    if ($train['action'] === 'update user') {
        unset($train['user']['roles']);
        unset($train['user']['referrers']);
        unset($train['user']['date_last_login']);
    }
 
    if ($train['action'] === 'delete user') {
        unset($train['user']['email_1']);
        unset($train['user']['user_token']);
        unset($train['user']['roles']);
    }
  
    return [$status_code, $train['user']];
}
