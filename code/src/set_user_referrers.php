<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Check as Check;
use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/**
 * Processes the user referrer input from the register user form.
 *
 * Automated assigning for referrers can be done in a subsequent microservice.
 *
 * @param   array  $train['user']
 *
 * @return  array  $train['user']
 */
function set_user_referrers(array $train): array
{
    if ($train['action'] == 'create user'
        && (! \array_key_exists('referrers', $train['users'])
            || \strlen($train['users']['referrers']) === 0)) {
        Messages\set_reply(400, ["The user referrers is not set in the registration form."]);

        return $train;
    }

    if (! \array_key_exists('user_name', $train['user'])
        || ! \array_key_exists('roles', $train['user'])) {
        return $train;
    }

    if ($train['action'] == 'create user') {
        if (\strpos($train['users']['referrers'], "&") !== false) {
            $ref_array = \explode("&", $train['users']['referrers']);
        } else {
            $ref_array = array($train['users']['referrers']);
        }

        $unique_referrers = [];
        $referrers = '';

        if (\count($ref_array) > 0) {
            foreach ($ref_array as $index => $ref) {
                if (\strpos($ref, "=") === false) {
                    Messages\set_reply(403, ["An = sign is missing from the referrers form input."]);
                } else {
                    $row = [];

                    list($referrer_type, $referrer_user_name) = \explode("=", $ref);

                    if (\strlen($referrer_user_name) === 0) {
                        Messages\set_reply(400, ["The [$referrer_type] referrer is set to blank."]);
                    } else {
                        // If user roles = Mastermind, do not search. Check that they all match
                        if (\stripos($train['user']['roles'], 'Mastermind') !== false) {
                            $referrer_user_name = $train['user']['user_name'];
                        } else {
                            $row = CrudTable\read_rows(
                                'users',
                                ["user_name" => $referrer_user_name],
                                1
                            );
          
                            if (\count($row) == 0) {
                                Messages\set_reply(
                                    404,
                                    ["The referrer [$referrer_user_name] is not found."]
                                );
                            }
                        }
                        // Allow the referrers that do not exist in the database to persist.
                        // That way, the missing referrer (users) can be added afterwards.
                        $unique_referrers[$referrer_type] = $referrer_user_name;
                    }
                }
            }

            $required_referrers = [
                'Mastermind',
                'Module',
                'Manager',
                'Marketer',
                'Motivator'
            ];

            foreach ($required_referrers as $index => $type) {
                if (! \array_key_exists($type, $unique_referrers)) {
                    Messages\set_reply(400, ["The [$type] referrer is not set in the registration form."]);
                }
            }

            foreach ($unique_referrers as $type => $user) {
                $referrers .= "$type=$user&";
            }

            $train['user']['referrers'] = \rtrim($referrers, "&");
        }
    } elseif (! \array_key_exists('referrers', $train['user'])) {
        Messages\set_reply(403, ["The referrers for the user have not been saved correctly."]);
    }

    return $train;
}
