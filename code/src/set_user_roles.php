<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Check as Check;
use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/**
 * Processes the roles input from the register user form.
 *
 * Automated assigning for referrers can be done in a subsequent microservice.
 *
 * @param   array  $train  The train['users'] with the user's referrers
 *
 * @return  array  $train['user']
 */
function set_user_roles(array $train): array
{
    $roles = '';

    if ($train['action'] === 'create user'
        && (! \array_key_exists('roles', $train['users'])
            || \strlen($train['users']['roles']) === 0)) {
        Messages\set_reply(400, ["The user roles is not set in the registration form."]);

        return $train;
    }

    if ($train['action'] === 'create user') {
        $acceptable_roles = [
            'mastermind',
            'module',
            'manager',
            'marketer',
            'motivator',
            'member'
        ];

        if (\strpos($train['users']['roles'], "&") !== false) {
            $roles_array = \explode("&", $train['users']['roles']);
        } else {
            $roles_array = array($train['users']['roles']);
        }

        foreach ($roles_array as $index => $role) {
            if (! \in_array(\strtolower($role), $acceptable_roles)) {
                Messages\set_reply(400, ["The [$role] user role is unacceptable."]);
            } else {
                $roles .= "$role&";
            }
        }

        $train['user']['roles'] = \rtrim($roles, "&");
    }
    
    if ($train['action'] !== 'delete user'
        && (! isset($train['user']['roles'])
            || \strlen($train['user']['roles']) < 10)
            && \count(Messages\get_reply(400, 600, 1)) == 0) {
        Messages\set_reply(403, ["The user roles have not been saved correctly."]);
    }

    return $train;
}
