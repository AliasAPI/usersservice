<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

/**
 * Updates the user row in the users table while avoiding storing bad data.
 *
 * @param   array   $train
 *
 * @return  array   $train
 */
function update_user_row(array $train): array
{
    if (($train['action'] !== 'update user'
        && $train['action'] !== 'login user')
        || ! isset($train['user']['user_uuid'])
        || \strlen($train['user']['user_uuid']) === 0) {
        return $train;
    }

    $update_pairs = $train['user'];

    // Never update the following columns:
    unset($update_pairs['user_name']);
    unset($update_pairs['user_uuid']);
    unset($update_pairs['date_created']);

    $where_pairs = [
        'user_uuid' => $train['user']['user_uuid']
    ];

    // If there ARE errors
    if (\count(Messages\get_reply(400, 600, 1)) > 0) {
        // Unset all of the columns to prevent bad data from being saved
        $update_pairs = [];

        // Specify the columns should be updated to track various errors
        $update_columns = [
            'json',
            'user_token',
            'date_modified',
            'date_last_email'
        ];

        foreach ($update_columns as $index => $column) {
            if (isset($train['user'][$column])) {
                $update_pairs[$column] = $train['user'][$column];
            }
        }
    }

    if (\count($update_pairs) > 0) {
        $affected = CrudTable\update_rows('users', $update_pairs, $where_pairs);

        Messages\set_reply(200, ["Updated table [$affected] rows."]);
        
        if (\count(Messages\get_reply(400, 600, 1)) === 0) {
            $train['user'] = CrudTable\read_rows('users', $where_pairs, 1);
        }
    }

    return $train;
}
