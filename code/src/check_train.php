<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Messages as Messages;

/**
 * This will check if the $train has errors
 *
 * @param   array  $train  The array to check
 *
 * @return  array  $train  Or, it returns a respond() on error
 */
function check_train(array $train): array
{
    if (! \array_key_exists('action', $train)
        || $train['action'] == '') {
        Messages\respond(400, ["The request did not include the action command."]);
    // todo:: Figure out if this pair check is actually needed. Is all pair stuff done?
    } elseif (! \array_key_exists('pair', $train)
              || count($train['pair']) === 0) {
        Messages\respond(400, ["The train did not include the pair attributes."]);
    // Note: CrudPair requests have already been processed and provided responses
    } elseif (! \array_key_exists('users', $train)) {
        Messages\respond(400, ["The train did not include the users attributes."]);
    }

    return $train;
}
