<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Check as Check;
use AliasAPI\Construe as Construe;
use AliasAPI\Messages as Messages;

/**
 * Sets the user_uuid based on the alias, namespace uuid, and immutable user_name
 * This way, the user_uuid can be recreated in the future using the same method
 *
 * @param   array  $train['user']['user_uuid']
 *
 * @return  array  $train
 */
function set_user_uuid(array $train): array
{
    if ($train['action'] === 'create user'
        && \array_key_exists('alias', $train['alias_attributes'])
        && $train['alias_attributes']['alias'] !== ''
        && \array_key_exists('service_uuid', $train['alias_attributes'])
        && $train['alias_attributes']['service_uuid'] !== ''
        && \array_key_exists('user_name', $train['user'])
        && $train['user']['user_name'] !== ''
        && \array_key_exists('email_1', $train['user'])
        && $train['user']['email_1'] !== ''
        // Do NOT set the user_uuid if there are any registraition errors
        &&  \count(Messages\get_reply(400, 600, 1)) === 0
        // Do NOT set the user_uuid if it has been previously set
        && (! \array_key_exists('user_uuid', $train['user'])
            || $train['user']['user_uuid'] == '')
        && Check\check_uuid(
            'service_uuid',
            $train['alias_attributes']['service_uuid'],
            []
        )) {
        // Create a uuid for a new user that is not in the database
        $train['user']['user_uuid'] = Construe\create_uuid(
            $train['alias_attributes']['alias'],
            $train['alias_attributes']['service_uuid'],
            $train['user']['user_name']
        );
    }
    
    if ($train['action'] !== 'delete user'
        && \array_key_exists('user_name', $train['user'])
        && ! \array_key_exists('user_uuid', $train['user'])) {
        Messages\respond(501, ["A user_uuid has not been saved yet."]);
    }

    return $train;
}
