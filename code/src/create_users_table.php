<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\CrudTable as CrudTable;
use AliasAPI\Messages as Messages;

function create_users_table(array $train): array
{
    if ($train['action'] !== 'setup services') {
        return $train;
    }

    $sql = "CREATE TABLE IF NOT EXISTS `users` (
            `email_1` VARCHAR(100) NOT NULL,
            `user_name` VARCHAR(36) NOT NULL,
            `user_uuid` VARCHAR(36) NOT NULL,
            `email_2` VARCHAR(100) NOT NULL,
            `roles` VARCHAR(255) NOT NULL,
            `referrers` VARCHAR(255) NOT NULL,
            `deleted` TINYINT(1) NOT NULL,
            `user_sign` longtext CHARACTER SET utf8mb4
                COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`json`)),
            `user_token` VARCHAR(100) NOT NULL,
            `json` longtext CHARACTER SET utf8mb4
                COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`json`)),
            `delay_email` INT(10) DEFAULT 0,
            `date_created` DATETIME NOT NULL,
            `date_modified` DATETIME NOT NULL,
            `date_last_login` DATETIME NOT NULL,
            `date_last_email` DATETIME NOT NULL,
            `date_email_1_verified` DATETIME NOT NULL,
            `date_email_2_verified` DATETIME NOT NULL,
            PRIMARY KEY (`user_uuid`(36)),
            UNIQUE KEY `email_1` (`email_1`(100)),
            UNIQUE KEY `user_name` (`user_name`(36))
            ) ENGINE=MyISAM";

    CrudTable\query($sql);

    Messages\respond(201, ["UsersService setup is complete."]);

    return $train;
}
