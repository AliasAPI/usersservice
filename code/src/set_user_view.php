<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Messages as Messages;

/**
 *
 * Set the view (page) that the website (frontend) should display for the user.
 *
 * create-user              Create an account using a user_name and email address.
 * confirm-registration     Confirm the registration user_name and email is correct.
 * accept-service-terms     Accept the terms of service for the site.
 * activate-account         Log into the site for the first time.
 * set-password             Set a password using the recovery key.
 * log-in                   Log in using the user account password.
 * update-profile           Update the first and last name on the account.
 * delete-user              Delete a user account
 *
 * If all the views are completed, do not return a view.
 * base, demo, error, success
 * skip, delay ?
 *
 * @param   array  $train
 *
 * @return  array  $train
 */
function set_user_view(array $train): array
{
    $errors = Messages\get_reply(400, 600, 1);

    if ($train['action'] === 'create user') {
        if (\count($errors) > 0) {
            $train['user']['view'] = "create-user-error";
        } else {
            $train['user']['view'] = "create-user-exit";
        }
    } elseif ($train['action'] === 'login user') {
        if (\count($errors) > 0) {
            $train['user']['view'] = "login-user-error";
        } else {
            // Tell the user to check their email
            $train['user']['view'] = "login-user-exit";
        }
    } elseif ($train['action'] === 'update user') {
        if (\count($errors) > 0) {
            $train['user']['view'] = "update-user-error";
        } elseif (! \array_key_exists('email_2', $train['user'])
                  || \strlen($train['user']['email_2']) == 0) {
            $train['user']['view'] = "update-user-profile-edit";
        } else {
            $train['user']['view'] = "update-user-exit";
        }
    } elseif ($train['action'] == 'delete user') {
        if (\count($errors) > 0) {
            $train['user']['view'] = "delete-user-error";
        } else {
            $train['user']['view'] = "delete-user-exit";
        }
    }

    // Only set a view when UsersService wants more data?
    
    return $train;
}
