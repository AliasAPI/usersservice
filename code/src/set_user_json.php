<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\CrudJson as CrudJson;
use AliasAPI\CrudTable as CrudTable;

/**
 * Set the user_json to be saved in the database
 * The json column dynamically stores extra data
 * that does not have a table column yet.
 *
 * @param array $train
 *
 * @return array $train
 */
function set_user_json(array $train): array
{
    $data = [];

    $columns = CrudTable\get_table_columns('users');

    // Remove the data that is stored in columns
    foreach ($train['user'] as $key => $value) {
        // If the key has been set in $train['user']
        // but is NOT defined in the columns array
        if (! \in_array($key, $columns)) {
             // Add the key value pair to $data
             $data[$key] = $train['user'][$key];

             // Remove the key pair
             unset($train['user'][$key]);
        }
    }

    // Encode and remove formatting & spaces to reduce data
    $train['user']['json'] = CrudJson\encode_json($data, '');

    return $train;
}
