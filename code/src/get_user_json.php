<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\CrudJson as CrudJson;

/**
 * Get the user json and add it to the User
 *
 * @param   array  $train
 *
 * @return  array  $train
 */
function get_user_json(array $train): array
{
    // Add the json 'columns' to the User
    if (isset($train['user']['json'])) {
        $data = CrudJson\decode_json($train['user']['json']);

        foreach ($data as $key => $value) {
            $train['user'][$key] = $value;
        }
    }

    return $train;
}
