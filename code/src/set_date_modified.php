<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Messages as Messages;

/**
 *
 *
 * @param   array   $train
 *
 * @return  array   $train
 */
function set_date_modified(array $train): array
{
    if ($train['action'] === 'create user'
        ||  $train['action'] === 'update user'
        ||  $train['action'] === 'delete user'
        ||  $train['action'] === 'login user') {
        $train['user']['date_modified'] = \gmdate('Y-m-d H:i:s', \time());
    }
    
    if (! \array_key_exists('date_modified', $train['user'])
        || $train['user']['date_modified'] == 0) {
        Messages\set_reply(501, ["A user date_modified is required for every user."]);
    }

    return $train;
}
