<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Crypto as Crypto;
use AliasAPI\Messages as Messages;

/**
 * Checks the User's signature (Javascript fingerprint)
 *
 * @param array $train
 *
 * @return array $train
 */
function set_user_token_claims(array $train): array
{
    // IF user enters email_2
    // user_sign matches?


    // Subject
    // A case-sensitive string containing a StringOrURI value.
    // The subject MUST either be scoped to be locally unique
    // in the context of the issuer or be globally unique
    $train['token_claims']['sub'] = $train['user']['user_name'];

    // Audience
    // An array of case-sensitive strings
    // Each entity intended to process the token MUST identify itself
    // with a value in the audience claim. If the entity cannot find
    // itself in the aud array, the token MUST be rejected.
    $train['token_claims']['aud'] = ['all'];

    // Set default iss, exp, jti claims
    $train['token_claims'] = 
        Crypto\set_paseto_token_claims($train['token_claims']);

    return $train;
}
