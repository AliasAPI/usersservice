<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Messages as Messages;

/**
 * Set the last time the User logged in successfully
 * The user_token is NOT set on unsuccessful logins
 *
 * @param array $train
 *
 * @return array $train
 */
function set_date_last_login(array $train): array
{
    if (\array_key_exists('user_token', $train['user'])
        && \strlen($train['user']['user_token']) > 5) {
        $train['user']['date_last_login'] = \gmdate('Y-m-d H:i:s', \time());
    } elseif (! \array_key_exists('date_last_login', $train['user'])
              || \strlen($train['user']['date_last_login']) < 5) {
              $train['user']['date_last_login'] = '0000-00-00 00:00:00';
    }
    
    return $train;
}
