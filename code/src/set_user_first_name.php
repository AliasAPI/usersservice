<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Check as Check;
use AliasAPI\Messages as Messages;

/**
 * Set the user's first_name
 *
 * @param   array   $train['user']
 *
 * @return  array   $train['user']['first_name']
 */
function set_user_first_name(array $train): array
{
    if (isset($train['users']['first_name'])) {
        $train['user']['first_name'] = $train['users']['first_name'];
    } elseif (! isset($train['user']['first_name'])) {
        $train['user']['first_name'] = '';
    }

    return $train;
}
