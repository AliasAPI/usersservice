<?php

declare(strict_types=1);

namespace AliasAPI\Users;

use AliasAPI\Check as Check;
use AliasAPI\Users as Users;
use AliasAPI\Messages as Messages;

function set_identity(array $train): array
{
    if ($train['action'] === 'setup services') {
        return $train;
    }

    if (! \array_key_exists('users', $train)) {
        Messages\set_reply(501, ["The users element was not set in the request."]);
    }

    $ids = ['user_token', 'user_name', 'user_uuid', 'email_1', 'email_2'];

    foreach ($ids as $index => $id) {
        if (\array_key_exists($id, $train['users'])
            && \strlen($train['users'][$id]) > 0) {
            // Expect the user_token to be included in each request
            if ($id === 'user_token') {
                // The user_token has already been checked via set_inputs()
                $user_token = \explode('-', $train['users']['user_token']);

                $train['user']['user_name'] = $user_token[0];
            } else {
                $train['user'][$id] = $train['users'][$id];
            }

            continue;
        }
    }

    if (! \array_key_exists('user', $train)
        || \count($train['user']) === 0) {
        Messages\set_reply(401, ["A user_name, email, user_token, or user_uuid is required."]);

        list($status_code, $response) = Users\set_response($train);

        Messages\respond($status_code, $response);
    }
    
    return $train;
}
