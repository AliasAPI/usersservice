<?php

namespace AliasAPI\Users;

use AliasAPI\Users as Users;
use AliasAPI\Server as Server;

require_once('../vendor/aliasapi/frame/server/bootstrap.php');

$train = Server\bootstrap();

$train = Server\track($train);

Users\track($train);
